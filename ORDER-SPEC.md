# Open Order Specification

Version: 0.1

The purpose of this specification is to outline a data structure for an **order** that is to be fulfilled from *one or more suppliers* and *delivered* by some method of transport. This specification exists to facilitate interoperability between different software platforms handling different parts of logistics in small to medium sized supply chains.

Orders MUST contain at minimum an `order_id` a `destination` address and at least one entry in the `items` list.

Systems COULD implement values that have human decipherable meanings- for instance the supplier `ber-ou-1` can be understood as `Berlin, Original Unberpackt #1` to represent a store.

```
{
    "order_id": "X4j6v2",
    "status": "collecting",
    "note": "Hallo. Please only deliver in late afternoons or such",
    "transport": "ecologistik",
    "date_dispatch": "2020-03-21",
    "date_delivery": "2020-03-22",
    "pickup": "Some Strasse 123, Berlin",
    "destination": "Other Weg 456, Berlin",
    "suppliers": [
        "ber-ou-1",
        "ber-kugu-1"
    ],
    "items": [
        {
            "supplier": "ber-ou-1",
            "item": "rice-white",
            "size": "md",
            "quantity": "2",
            "cost": "1.80",
            "status": "ordered"
        },{
            "supplier": "ber-ou-1",
            "item": "beans-black",
            "size": "md",
            "quantity": "2",
            "cost": "1.65",
            "status": "ordered"
        },{
            "supplier": "ber-kugu-1",
            "item": "peanuts-sesame",
            "size": "sm",
            "quantity": "1",
            "cost": "2.15",
            "status": "packed"
        },{
            "supplier": "ber-enk-1",
            "item": "soap-hand-bar-organic",
            "size": "single",
            "quantity": "2",
            "cost": "3.50",
            "status": "ordered"
        }
    ]
}
```

### Order Fields

**order.order_id**

*Alphanum* - This is a global value to keep track of an order. It will be seen by the person placing order, by the packer, transport, and any support people involved. This is value is non-editable.

**order.status**

*String* - this value is something that auto and manually updates throughout the *"collection"* and *"delivery"* phases of the process. The following values are those which will update over time.

- placed
- cancelled
- collecting
- collected
- transit
- delivered
- incomplete

**date_dispatch**

*String* - a date value representing when an order added to a dispatch system.

**date_delivery**

*String* - a date value representing when an order is expected to be delivered by.

*Both date values must be formatted in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) with a minimum of **date** `2020-03-21` but can also be **date and time** such as `2020-03-21T20:24:08+00:00`*

**order.pickup** - *optional*

*String* - standard formatted address that can be easily looked up on a map.

**order.destination**

*String* - standard formatted address that can be easily looked up on a map.

**order.note** - *optional*

*String* - an optional field for any special information that needs to be communicated to a deliverer or supplier.

**order.transport**

*String* - an ID field representing who will be doing the delivery or transportion of a items.

**order.suppliers**

*Array* - this is a list of supplier IDs that a packer can use to use to filter through when they are at a given shop or depot to see only which items are needed.

### Items

The `order.items` list represents one or more "items" that are being collected for transportation.

**item.status**

*String* - a value used to track specific items in a given order and their state.

- ordered
- packed
- damaged
- delivered
- out-of-stock
