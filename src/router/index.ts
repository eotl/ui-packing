import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "home" */ "../views/Auth.vue"),
    // Child route "tabs"
    children: [
      // Note we provide the above parent route name on the default child tab
      // route to ensure this tab is rendered by default when using named routes
      {
        path: "",
        name: "Sign In",
        component: () =>
          import(/* webpackChunkName: "signin" */ "../components/Signin.vue"),
      },
      {
        path: "signin",
        name: "Sign In",
        component: () =>
          import(/* webpackChunkName: "signin" */ "../components/Signin.vue"),
      },
      {
        path: "register",
        name: "Register",
        component: () =>
          import(
            /* webpackChunkName: "register" */ "../components/Register.vue"
          ),
      },
    ],
  },
  {
    path: "/about",
    name: "About",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/orders",
    name: "Orders",
    component: () =>
      import(/* webpackChunkName: "orders" */ "../views/Orders.vue"),
  },
  {
    path: "/order/:orderId",
    name: "Order",
    component: () =>
      import(/* webpackChunkName: "order" */ "../components/Order.vue"),
  },
  {
    path: "/supplier/:supplierId",
    name: "Supplier",
    component: () =>
      import(/* webpackChunkName: "supplier" */ "../components/Supplier.vue"),
  },
  {
    path: "/suppliers",
    name: "Suppliers",
    component: () =>
      import(/* webpackChunkName: "suppliers" */ "../views/Suppliers.vue"),
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
