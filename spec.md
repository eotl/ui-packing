# Open Inventory Packing Schema

The following is a schema for an HTML5 / mobile (ReactNative) type of application that allows someone fulfilling an order (a packer, or delivery rider) to view a list of multiple items from multiple suppliers to "pack" in an order. A robust shopping list if you will.

https://codeberg.org/eotl/open-inventory-packing

## Existing Data sources

These are sources which are currently exist and come out of the "Open Inventory" API. This is the stuff presented by the "Inteface Ordering" which a customer or comrade will select what items they need and in which quantities.

https://zerowaste.civickit.eu/api/categories
https://zerowaste.civickit.eu/api/suppliers
https://zerowaste.civickit.eu/api/items/grains
https://zerowaste.civickit.eu/api/items/nuts-seeds

The result of browsing and selecting items in a "shopping cart" like fashion will have a data object that gets saved on server like

```
{
    "order_id": "X4j6v2",
    "status": "collecting",
    "note": "Hallo. Please only deliver in late afternoons or such",
    "pickup": "Some Strasse 123, Berlin",
    "destination": "Other Weg 456, Berlin",
    "suppliers": [
        "ber-ou-1",
        "ber-kugu-1"
    ],
    "items": [
        {
            "supplier": "ber-ou-1",
            "item": "rice-white",
            "size": "md",
            "quantity": "2",
            "cost": "1.80",
            "status": "ordered"
        },{
            "supplier": "ber-ou-1",
            "item": "beans-black",
            "size": "md",
            "quantity": "2",
            "cost": "1.65",
            "status": "ordered"
        },{
            "supplier": "ber-kugu-1",
            "item": "peanuts-sesame",
            "size": "sm",
            "quantity": "1",
            "cost": "2.15",
            "status": "packed"
        },{
            "supplier": "ber-enk-1",
            "item": "soap-hand-bar-organic",
            "size": "single",
            "quantity": "2",
            "cost": "3.50",
            "status": "ordered"
        }
    ]
}
```

order.order_id

Alphanum - This is a global value to keep track of an order. It will be seen by the person placing order, by the packer / delivery person, and any support people involved. This is value is non-editable.

order.status

String - this value is something that auto and manually updates throughout the "collection" and "delivery" phases of the process. The following values are those which will update over time.

```
placed
cancelled
collecting
collected
transit
delivered
incomplete
```

order.pickup

String - standard formatted address that can be easily looked up on a map

order.destination

String - standard formatted address that can be easily looked up on a map

order.note

Alphanum -

order.suppliers

Array - this is a list of supplier IDs that a packer can use to use to filter through when they are at a given shop or depot to see only which items are needed.

## item.status

String - a value used to track specific items in a given order and their state

```
ordered
packed
damaged
delivered
out-of-stock
```

---

## View: Orders

A main overview view of all orders a given packer / rider can see at a given moment

## View: Order Details

The specifics about a given order showing the number of suppliers, and items to be collected.

This shows the address of where to take delivery as well and provides a link to a map or something

## View: Item Details

Shows details about a specific item within an order "beans-black" for instance. The interface allows the rider to update the status of the item with extra values like "out-of-stock"

---

## User Stories

- i want to be able to see a list of orders
- i want to click on an order and see order details
- i want to click on an item within an order and see item details.

# tech stack:

- react
  - styled components
- typescript
  https://medium.com/javascript-in-plain-english/how-to-build-a-todo-list-app-with-react-hooks-and-typescript-b9cbdc61e966
- react testing library
  - use react-testing-library instead of enzyme
    - https://www.freecodecamp.org/news/8-simple-steps-to-start-testing-react-apps-using-react-testing-library-and-jest/
    - https://medium.com/@boyney123/my-experience-moving-from-enzyme-to-react-testing-library-5ac65d992ce
- graphql
  - apollo
